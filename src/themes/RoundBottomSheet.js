import React, { PureComponent } from 'react'
import { Image, Modal, TouchableOpacity, TouchableWithoutFeedback, StyleSheet } from 'react-native'
import { DEVICE_HEIGHT, COLORS } from '~/src/themes/common'
import View from './View'
import Text from './Text'
import * as Animatable from 'react-native-animatable'
const POPUP_CONTENT_HEIGHT =  DEVICE_HEIGHT - 139
const SLIDE_DISTANCED = POPUP_CONTENT_HEIGHT + 20

export default class RoundBottomSheet extends PureComponent {

    constructor(props) {
        super(props)
        Animatable.initializeRegistryWithDefinitions({
            slideInUp: {
                from: {
                    translateY: SLIDE_DISTANCED,
                },
                to: {
                    translateY: 0,
                },
            },
            slideOutDown: {
                from: {
                    translateY: 0,
                },
                to: {
                    translateY: SLIDE_DISTANCED,
                },
            }
        })
    }

    _closeModal = () => {
        this.animatedView && this.animatedView.slideOutDown(700)
            .then(() => {
                const { onClose } = this.props
                onClose && onClose()
            })

    }

    render() {
        const { visible, children, title,fixSize } = this.props

        return (
            <Modal
                animationType={'none'}
                visible={visible}
                transparent={true}
                onRequestClose={this._closeModal}
            >
                <TouchableWithoutFeedback onPress={this._closeModal}>
                    <View style={styles.backdrop}>
                        <Animatable.View
                            style={styles.popupOuter}
                            useNativeDriver={true}
                            easing={'ease-in-out'}
                            duration={700}
                            animation={'slideInUp'}
                            ref={ref => this.animatedView = ref}
                        >
                            <View style={[styles.popupContent,{height:fixSize?'auto':POPUP_CONTENT_HEIGHT}]}>
                                <View className='row-center'>
                                    <View style={styles.popupHeaderIcon} />
                                </View>
                                <View style={styles.popupTitleContainer}>
                                    <Text style={styles.popupTitle}>{title}</Text>
                                    <TouchableOpacity onPress={this._closeModal}>
                                        {/* <Image source={require('~/src/image/chevron_down_light.png')}
                                            style={styles.iconDown}
                                        /> */}
                                        <Text>Close</Text>
                                    </TouchableOpacity>
                                </View>
                                {children}
                            </View>
                        </Animatable.View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    backdrop: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        backgroundColor: COLORS.BACKDROP
    },
    popupHeaderIcon: {
        width: 70,
        height: 4,
        borderRadius: 2,
        backgroundColor: COLORS.BORDER_COLOR,
        marginTop: 8
    },
    popupContent: {
        backgroundColor: COLORS.WHITE,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        height: POPUP_CONTENT_HEIGHT,
        flex: 1,
    },
    popupOuter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    popupTitleContainer: {
        paddingTop: 22,
        paddingBottom: 14,
        flexDirection: 'row',
        paddingHorizontal: 24,
        borderBottomWidth: 1,
        borderBottomColor: COLORS.BORDER_COLOR2
    },
    popupTitle: {
        lineHeight: 20,
        flex: 1
    },
    iconDown: {
        width: 24,
        height: 24
    }
})