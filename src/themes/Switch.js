import React from 'react'
import { StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { COLORS } from './common'
import View from './View'
import * as Animatable from 'react-native-animatable'

export default Switch = (props) => {
    const { enable, onPress } = props
    const backgroundColor = enable ? COLORS.CERULEAN : COLORS.BORDER_COLOR
    _handlePress = () => {
        onPress && onPress()
    }
    return (
        <TouchableWithoutFeedback onPress={_handlePress}>
            <View className={'row-start'}
                style={[styles.container, { backgroundColor }]}
            >
                <Animatable.View
                    transition={'translateX'}
                    duration={200}
                    useNativeDriver={true}
                    easing={'ease-in-out'}
                    style={{
                        ...styles.indicator,
                        translateX: enable ? 32 : 0
                    }} />
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 64,
        height: 32,
        borderRadius: 16,
        padding: 4
    },
    indicator: {
        height: 24,
        width: 24,
        borderRadius: 12,
        backgroundColor: COLORS.WHITE
    }
})

