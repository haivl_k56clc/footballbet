import React, { Fragment, Component } from 'react';
import {
    SafeAreaView,
    FlatList,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';
import { View, Text } from '~/src/themes/ThemeComponent'
import firebase from 'react-native-firebase';
import { COLORS } from '~/src/themes/common';
import moment from 'moment'
import I18n from '~/src/I18n'

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.matches = firebase.firestore().collection('Matches');
        this.state = {
            matches: []
        }
    }

    componentDidMount() {
        // this.unsubscribe = this.matches.onSnapshot(this.onCollectionUpdate)
        this.matches.orderBy('startTime', 'desc').onSnapshot(this.onCollectionUpdate)
    }

    componentWillUnmount() {
        this.unsubscribe && this.unsubscribe()
    }

    onCollectionUpdate = (querySnapshot) => {
        let matches = []
        querySnapshot.forEach((doc) => {
            console.log("Matches: ", doc.id, " => ", doc.data());
            matches.push({
                id: doc.id,
                ...doc.data()
            })
            this.setState({ matches })
        });
    }

    _renderMatchItem = ({ item, index }) => {
        console.log('_renderMatchItem', item)
        return (
            <TouchableOpacity>
                <View className='mh8 pv16 white' style={styles.matchItem}>
                    <View className='row-center mb8'>
                        <Text className='s12 center gray'>{moment(item.startTime * 1000).format(I18n.t('full_date_time_format'))}</Text>
                    </View>
                    <View className='row-start'>
                        <View className='flex column-center'>
                            <Image
                                source={{ uri: item.teams[0].logo }}
                                style={styles.logo}
                            />
                            <Text className='s16 bold textBlack'>{item.teams[0].name}</Text>
                        </View>
                        <Text style={styles.vs}>vs</Text>
                        <View className='flex column-center'>
                            <Image
                                source={{ uri: item.teams[1].logo }}
                                style={styles.logo}
                            />
                            <Text className='s16 bold textBlack'>{item.teams[1].name}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {

        return (
            <Fragment>
                <SafeAreaView style={{ flex: 1 }}>
                    <View className='background flex'>
                        <View className='space16' />
                        <FlatList
                            data={this.state.matches}
                            renderItem={this._renderMatchItem}
                            keyExtractor={item => item.id + ''}
                        />
                    </View>
                </SafeAreaView>
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    safeView: {
        flex: 1
    },
    matchItem: {
        borderRadius: 6,
        marginBottom: 8,
        marginHorizontal: 8,
        paddingVertical: 16
    },
    logo: {
        width: 60,
        height: 60,
        marginBottom: 8,
        backgroundColor: COLORS.WHITE
    },
    vs: {
        color: COLORS.CERULEAN,
        fontWeight: 'bold',
        fontSize: 24
    }
})